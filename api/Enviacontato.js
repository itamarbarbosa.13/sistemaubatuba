$(document).ready(function(){
    $("#wpcf7-cf7sg-form-contato").submit(function(event){
        event.preventDefault();

        $('.wpcf7-submit').prop('disabled', true);
        $("#respostaform").empty();

        var $form = $(this),
        nome = $form.find("input[name='nome']").val(),
        email = $form.find("input[name='email-759']").val(),
        endereco = $form.find("input[name='Endereo']").val(),
        tel1 = $form.find("input[name='Tel1']").val(),
        tel2 = $form.find("input[name='Tel2']").val(),
        mensagem = $form.find("textarea[name='x5']").val();

        $.ajax({
            url: "https://api.ecoubatuba.com.br/api/email/contato?empresa=1",
            datatype: 'json',
            data: JSON.stringify({
                "email": email,
                "nome": nome,
                "endereco": endereco,
                "telefones": [ tel1, tel2 ],
                "mensagem": mensagem
            }),
            type: 'POST',
            contentType: 'application/json',
            success: function(data){
                $('.wpcf7-submit').prop('disabled', false);
                $("#respostaform").append("<h2 style='text-align: center;font-size: 20px;'>Email enviado com sucesso</h2>");

                setTimeout(function() {
                    $("#respostaform").empty();
                }, 10000);
            },
            error: function(data) {
                var mensagem = 'Falha ao enviar mensagem';
                if (data && data.responseJSON && data.responseJSON.mensagem) {
                    mensagem = data.responseJSON.mensagem;
                }
                $('.wpcf7-submit').prop('disabled', false);
                $("#respostaform").append("<h2 style='text-align: center;font-size: 20px;'> " + mensagem + "</h2>");    

                setTimeout(function() {
                    $("#respostaform").empty();
                }, 10000);
            }
        })
        
    })
    
})


